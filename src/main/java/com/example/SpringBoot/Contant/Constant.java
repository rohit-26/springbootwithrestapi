package com.example.SpringBoot.Contant;

public class Constant {

	public static final String CREATE_MESSAGE="RESOURCE HAS BEEN CREATED WITH ID:";
	public static final String USER_FOUND="USER FOUND";
	public static final String USER_NOT_FOUND="USER NOT FOUND";
	public static final int ZERO=0;
	public static final String PROFILE_CREATED="PROFILE CREATED";
	public static final String PROFILE_NOT_CREATED="PROFILE NOT CREATED";
	
}
