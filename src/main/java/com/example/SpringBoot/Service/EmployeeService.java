package com.example.SpringBoot.Service;

import java.util.List;

import com.example.SpringBoot.Entity.Authentication;
import com.example.SpringBoot.Entity.PracticeData;
import com.example.SpringBoot.Entity.Result;

public interface EmployeeService {

	public List<PracticeData> getAllEmployee();
	public Result saveEmplDetail(PracticeData employee);
	public PracticeData getEmployee(int employeeID);
	public PracticeData getEmployeeDetail(String empName); 
	public Authentication loginAuth(String username, String password);
	
	
}
