package com.example.SpringBoot.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.SpringBoot.Contant.Constant;
import com.example.SpringBoot.DAO.CrudOperation;
import com.example.SpringBoot.Entity.Authentication;
import com.example.SpringBoot.Entity.PracticeData;
import com.example.SpringBoot.Entity.Result;
import com.example.SpringBoot.Exception.StudentNotFoundException;
import com.example.SpringBoot.Service.EmployeeService;


@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	private CrudOperation employeeDAO;
	
	
	
	@Override
	public List<PracticeData> getAllEmployee() {
		return employeeDAO.getAllEmployees();
	}

	@Override
	public Result saveEmplDetail(PracticeData employee) {
		Result resultMessage= new Result();
		int result=employeeDAO.createEmployee(employee);
		if(result>Constant.ZERO)
			 resultMessage.setResult(Constant.PROFILE_CREATED);
		else
			resultMessage.setResult(Constant.PROFILE_NOT_CREATED);
		
		return resultMessage;
	}

	@Override
	public PracticeData getEmployee(int employeeID) {
		
		if((employeeID>=employeeDAO.getAllEmployees().size()) || (employeeID<0))
		{
			throw new StudentNotFoundException("Student id not found -"+employeeID);
		}
		return employeeDAO.getEmployee(employeeID);
	}

	@Override
	public Authentication loginAuth(String username, String password) {
		Authentication auth= new Authentication();
		long result=employeeDAO.loginUser(username, password);
		System.out.println("RESULT: "+result);		
		
		 if(result>0)
			 auth.setAuthentication(Constant.USER_FOUND);
			 
		 else
			 auth.setAuthentication(Constant.USER_NOT_FOUND);
			
			 
		return auth;
	}

	@Override
	public PracticeData getEmployeeDetail(String empName) {
		
		return employeeDAO.getEmployeeDetails(empName);
	}

	
	
}
