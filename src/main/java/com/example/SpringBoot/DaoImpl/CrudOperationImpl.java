package com.example.SpringBoot.DaoImpl;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.SpringBoot.Contant.Constant;
import com.example.SpringBoot.DAO.CrudOperation;
import com.example.SpringBoot.Entity.PracticeData;


@Repository
public class CrudOperationImpl implements CrudOperation {

	
	@Autowired
	private EntityManagerFactory entityManagerFactory;
	
	@Override
	public List<PracticeData> getAllEmployees() {
		
		Session session =null;
		session= entityManagerFactory.unwrap(SessionFactory.class).openSession();
		List<PracticeData> employeeList=session.createQuery("from PracticeData").list();
		session.close();
		return employeeList;
	}

	@Override
	public int createEmployee(PracticeData practice) {
	Session session =null;
	Transaction transaction;
	 
		session = entityManagerFactory.unwrap(SessionFactory.class).openSession();;
		transaction= session.beginTransaction();
		Integer id=(Integer)session.save(practice);
		
		transaction.commit();
		System.out.println("Employee is created with ID:"+practice.getEmpid());
		session.close();
		
	return id;
		
	}

	@Override
	public PracticeData getEmployee(int employeeid) {
				
		Session session =null;
		PracticeData p = new PracticeData();
		 
		session = entityManagerFactory.unwrap(SessionFactory.class).openSession();
		Query query=session.createQuery("from PracticeData where empid= :employeeID");
		query.setParameter("employeeID", employeeid);
		
		p=(PracticeData) query.getSingleResult();
		
		session.close();
		return p;
	}

	@Override
	public long loginUser(String username, String password) {
		
		Session session =null;
		PracticeData p = new PracticeData();
		 
		session = entityManagerFactory.unwrap(SessionFactory.class).openSession();
		
		Query query= session.createQuery("select count(*) from PracticeData where empname= :empName and emppassword=:empPass");
		query.setParameter("empName", username);
		query.setParameter("empPass", password);
		
		long dataFound=(Long)query.getSingleResult();
		session.close();				
		return dataFound;
	}

	@Override
	public PracticeData getEmployeeDetails(String empName) {
		Session session =null;
		PracticeData p = new PracticeData();
		 
		session = entityManagerFactory.unwrap(SessionFactory.class).openSession();
		Query query=session.createQuery("from PracticeData where empname= :emplName");
		query.setParameter("emplName", empName);
		
		p=(PracticeData) query.getSingleResult();
		
		session.close();
		return p;
		
	}

	
	
}
