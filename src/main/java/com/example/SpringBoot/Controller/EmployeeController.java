package com.example.SpringBoot.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.SpringBoot.Entity.Authentication;
import com.example.SpringBoot.Entity.LoginUser;
import com.example.SpringBoot.Entity.PracticeData;
import com.example.SpringBoot.Entity.Result;
import com.example.SpringBoot.Exception.StudentDetailNotFoundResponse;
import com.example.SpringBoot.Exception.StudentNotFoundException;
import com.example.SpringBoot.Service.EmployeeService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService employeeServ;
	/* 
	 * 1st way to pass data to 
	 */
	@RequestMapping(value="/employees", method=RequestMethod.GET)
	@ResponseBody
	public List<PracticeData> getAllEmployee()
	{
		return employeeServ.getAllEmployee();
	}
	
	
	@RequestMapping(value="/employees/{employeeID}", method=RequestMethod.GET)
	public PracticeData getEmployee(@PathVariable int employeeID)
	{	
		return employeeServ.getEmployee(employeeID);
	}
	
	@RequestMapping(value="/employeeDetail/{empName}",  produces="application/json", method=RequestMethod.GET)
	public PracticeData getEmployeeDetail(@PathVariable String empName)
	{	
		return employeeServ.getEmployeeDetail(empName);
	}
	
	

	@RequestMapping(value="/employeesList", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<PracticeData> getEmployees()
	{
		List<PracticeData> empDetails= employeeServ.getAllEmployee();
		return new ResponseEntity(empDetails, HttpStatus.OK);
	}
	
	
	@PostMapping(value="/createemployee")
	@ResponseBody
	public Result createEmployees(@RequestBody PracticeData employee )
	{
		return employeeServ.saveEmplDetail(employee);
	}
	
	
	@RequestMapping(value="/login", produces="application/json", method=RequestMethod.POST)
	@ResponseBody
	public Authentication loginAuth( @RequestBody LoginUser loginUserData)
	{
		return employeeServ.loginAuth(loginUserData.getUsername(), loginUserData.getPassword());
	
	}
	
	
	@ExceptionHandler	
	public ResponseEntity<StudentDetailNotFoundResponse> handleException(StudentNotFoundException  studentExcp)
	{
		StudentDetailNotFoundResponse st= new StudentDetailNotFoundResponse();
		st.setStatus(HttpStatus.NOT_FOUND.value());
		st.setMessage(studentExcp.getMessage());
		st.setTimestamp(System.currentTimeMillis());
		
		return new ResponseEntity<>(st, HttpStatus.NOT_FOUND);
		
	}
}
