package com.example.SpringBoot.DAO;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.SpringBoot.Entity.PracticeData;
import com.example.SpringBoot.Entity.Student;

@Repository
public interface CrudOperation {
	
	public List<PracticeData> getAllEmployees();
	public PracticeData getEmployee(int employeeid);
	public int createEmployee(PracticeData practice);
	public long loginUser(String username, String password);
	public PracticeData getEmployeeDetails(String empName);
	
}
