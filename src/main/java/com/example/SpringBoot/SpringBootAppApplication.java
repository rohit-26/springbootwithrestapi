package com.example.SpringBoot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.SpringBoot.DAO.CrudOperation;
import com.example.SpringBoot.Entity.PracticeData;

@SpringBootApplication
public class SpringBootAppApplication implements CommandLineRunner {

	@Autowired
	private CrudOperation crudOperation;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootAppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//PracticeData p=crudOperation.getEmployee(1);
		/*System.out.println(p.getEmpname());
		
		long loginUserCheck= crudOperation.loginUser("Praveen", "prav");
		System.out.println(loginUserCheck);*/
		
		//PracticeData p1=crudOperation.getEmployeeDetails("Rohit");
		
		//System.out.println(p.getEmpaddress());
		
	}
}
