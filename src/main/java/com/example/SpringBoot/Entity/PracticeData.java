package com.example.SpringBoot.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="practice")
public class PracticeData {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="empid")
	private int empid;
	private String empname;
	private String emppassword;
	private String empdept;
	private String empco;
	private String empaddress;
	private String emptechnology;
	@Transient
	private String empLoginMessage;
	
	public PracticeData() {}


	public int getEmpid() {
		return empid;
	}


	public void setEmpid(int empid) {
		this.empid = empid;
	}


	public String getEmpname() {
		return empname;
	}


	public void setEmpname(String empname) {
		this.empname = empname;
	}


	public String getEmppassword() {
		return emppassword;
	}


	public void setEmppassword(String emppassword) {
		this.emppassword = emppassword;
	}


	public String getEmpdept() {
		return empdept;
	}


	public void setEmpdept(String empdept) {
		this.empdept = empdept;
	}


	public String getEmpco() {
		return empco;
	}


	public void setEmpco(String empco) {
		this.empco = empco;
	}


	public String getEmpaddress() {
		return empaddress;
	}


	public void setEmpaddress(String empaddress) {
		this.empaddress = empaddress;
	}


	public String getEmptechnology() {
		return emptechnology;
	}


	public void setEmptechnology(String emptechnology) {
		this.emptechnology = emptechnology;
	}


	public String getEmpLoginMessage() {
		return empLoginMessage;
	}


	public void setEmpLoginMessage(String empLoginMessage) {
		this.empLoginMessage = empLoginMessage;
	}

	
	
	
	
}
