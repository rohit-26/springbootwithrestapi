package com.example.SpringBoot.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

//@Entity
public class Student {
	
	/*@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "student_Sequence")
    @SequenceGenerator(name = "student_Sequence", sequenceName = "STUDENT_SEQ")*/
	private int sid;
	
	//@Column(name="STUDENT")
	private String studentName;
	//@Column(name="CLASSNAME")
	private String className;
	//@ManyToOne( fetch=FetchType.LAZY)
	//@JoinColumn(name="course_id", nullable=false)
	private Course course;
	
	public Student() {}
	public int getSid() {
		return sid;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	
	
	
	
}
