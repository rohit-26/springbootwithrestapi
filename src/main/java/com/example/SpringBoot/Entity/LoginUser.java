package com.example.SpringBoot.Entity;

public class LoginUser {
	
	private String username;
	private String password;
	
	
	private LoginUser() {}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
