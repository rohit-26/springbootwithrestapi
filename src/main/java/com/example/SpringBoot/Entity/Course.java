package com.example.SpringBoot.Entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

//@Entity
public class Course {
	
	/*@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "course_Sequence")
    @SequenceGenerator(name = "course_Sequence", sequenceName = "COURSE_SEQ")*/
	private int cid;
	
	//@Column(name="COURSENAME")
	private String courseName;
	

    /*@OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            mappedBy = "course")*/
	private List<Student> students;
	
	public Course()
	{}
	

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public List<Student> getStudentName() {
		return students;
	}

	public void setStudentName(List<Student> students) {
		this.students = students;
	}
	
	
	

}
